﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Models.PublicModels;
using Repositories;
using Repositories.AnnouncementRepository;
using Repositories.ChannelRepository;
using Repositories.CourseRepository;
using Repositories.MessageRepository;
using Repositories.QuizRepository;
using Repositories.QuoteRepository;
using Repositories.VideoRepository;
using Services.AnnouncementService;
using Services.ChannelService;
using Services.CourseService;
using Services.MessageService;
using Services.QuestionService;
using Services.QuizService;
using Services.QuoteService;
using Services.UserService;
using Services.VideoService;
using UserClientApi.Helpers;

namespace UserClientApi
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {


            //Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IChannelService, ChannelService>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<IVideoService, VideoService>();
            services.AddScoped<IQuizService, QuizService>();
            services.AddScoped<IQuestionService, QuestionService>();
            services.AddScoped<IQuoteService, QuoteService>();
            services.AddScoped<IAnnouncementService, AnnouncementService>();
            services.AddScoped<IMessageService, MessageService>();

            //Repositories
            services.AddScoped<IUserRepository, UserRepository>(provider => new UserRepository("users", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<IChannelRepository, ChannelRepository>(provider => new ChannelRepository("channels", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<ICourseRepository, CourseRepository>(provider => new CourseRepository("courses", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<IVideoRepository, VideoRepository>(provider => new VideoRepository("videos", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<IQuizRepository, QuizRepository>(provider => new QuizRepository("quizes", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<IQuestionRepository, QuestionRepository>(provider => new QuestionRepository("questions", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<IQuoteRepository, QuoteRepository>(provider => new QuoteRepository("quotes", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<IAnnouncementRepository, AnnouncementRepository>(provider => new AnnouncementRepository("announcements", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));
            services.AddScoped<IMessageRepository, MessageRepository>(provider => new MessageRepository("messages", Environment.GetEnvironmentVariable("POSTGRES_DB_CONN_STRING")));


            //Helpers
            services.AddSingleton<BCryptPasswordHasher>();



            // allow all cors
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllCorsPolicy", builder =>
                {
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                    builder.AllowAnyOrigin();
                });
            });


            //Authentications
            services.AddAuthentication("BasicAuthentication")
            .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            //Enable Swagger
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "E-Learning API", Version = "v1" }); });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;


            //add Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("../swagger/v1/swagger.json", "E-Learning API V1"); });


            app.UseCors("AllowAllCorsPolicy");
            app.UseHttpsRedirection();

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseAuthentication();
            app.UseMvc();


        }


    }
}

