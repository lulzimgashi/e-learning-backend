using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Models.PublicModels.Exceptions;
using Newtonsoft.Json;

namespace UserClientApi.Helpers
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            string result;

            if (ex is BaseException)
            {
                BaseException baseException = ex as BaseException;
                result = JsonConvert.SerializeObject(new { type = baseException.type.ToString(), message = baseException.Message });
                context.Response.StatusCode = (int)baseException.code;
                System.Console.WriteLine("Exception Type = " + (ex as BaseException).type.ToString());

            }
            else
            {
                result = JsonConvert.SerializeObject(new { type = "ServerException", message = ex.Message });
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            System.Console.WriteLine("Exception Message = "+ex.Message);

            context.Response.ContentType = "application/json";
            return context.Response.WriteAsync(result);
        }
    }
}
