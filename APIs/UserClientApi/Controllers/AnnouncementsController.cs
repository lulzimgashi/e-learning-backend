
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.AnnouncementService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class AnnouncementsController : Controller
    {
        private readonly IAnnouncementService _announcementService;

        public AnnouncementsController(IAnnouncementService announcementService)
        {
            _announcementService = announcementService;
        }


        [HttpGet]
        public async Task<ActionResult<IList<Announcement>>> GetAllAnnouncements()
        {
            IList<Announcement> announcements = await _announcementService.GetAllAnnouncementsAsync();

            return Ok(announcements);
        }


        [HttpPost, Authorize]
        public async Task<ActionResult<Announcement>> AddNewAnnouncement([FromBody]Announcement announcement)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            announcement.userId = userId;


            announcement = await _announcementService.AddNewAnnouncementAsync(announcement);

            return Ok(announcement);
        }



        [HttpGet("{announcementId}")]
        public async Task<ActionResult<Announcement>> GetSingleAnnouncement(string announcementId)
        {
            Announcement announcement = await _announcementService.GetSingleAnnouncementAsync(announcementId);

            return Ok(announcement);
        }


        [HttpPut("save/{announcementId}"), Authorize]
        public async Task<IActionResult> SaveAnnouncement(string announcementId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _announcementService.SaveUserAnnouncementAsync(userId, announcementId);

            return Ok();
        }

        [HttpPut("unsave/{announcementId}"), Authorize]
        public async Task<IActionResult> UnSaveAnnouncement(string announcementId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _announcementService.UnSaveUserAnnouncementAsync(userId, announcementId);

            return Ok();
        }

    }

}