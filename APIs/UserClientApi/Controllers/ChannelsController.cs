using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.ChannelService;
using Services.UserService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class ChannelsController : Controller
    {
        private readonly IChannelService _channelService;

        public ChannelsController(IChannelService channelService)
        {
            _channelService = channelService;
        }


        [HttpGet]
        public async Task<ActionResult<IList<Channel>>> GetAllChannels()
        {
            IList<Channel> channels = await _channelService.GetAllChannelsAsync();

            return Ok(channels);
        }


        [HttpGet("admin"), Authorize(Roles = "ADMIN ")]
        public async Task<ActionResult<IList<Channel>>> GetAllChannelsAdmin()
        {
            var adminId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            IList<Channel> channels = await _channelService.GetAllChannelsAdminAsync(adminId);

            return Ok(channels);
        }


        [HttpPost, Authorize(Roles = "ADMIN ")]
        public async Task<ActionResult<Channel>> CreateChannel([FromBody] Channel channel)
        {
            var adminId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            channel.userId = adminId;

            Channel savedChannel = await _channelService.CreateChannelAsync(channel);

            return Ok(savedChannel);
        }


        [HttpPut, Authorize(Roles = "ADMIN ")]
        public async Task<ActionResult<Channel>> UpdateChannel([FromBody] Channel channel)
        {
            var adminId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            channel.userId = adminId;

            Channel savedChannel = await _channelService.UpdateChannelAsync(channel);

            return Ok(savedChannel);
        }



        [HttpGet("{channelId}")]
        public async Task<ActionResult<Channel>> GetSingleChannel(string channelId)
        {
            Channel channel = await _channelService.GetSingleChannelAsync(channelId);

            return Ok(channel);
        }


        [HttpDelete("{channelId}"), Authorize(Roles = "ADMIN ")]
        public async Task<IActionResult> DeleteChannel(string channelId)
        {
            var adminId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _channelService.DeleteChannelAsync(channelId, adminId);

            return Ok();
        }

    }

}