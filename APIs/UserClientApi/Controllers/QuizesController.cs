using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.QuizService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class QuizesController : Controller
    {
        private readonly IQuizService _quizService;

        public QuizesController(IQuizService quizService)
        {
            _quizService = quizService;
        }


        [HttpGet("video/{videoId}"), Authorize]
        public async Task<ActionResult<Quiz>> GetSingleCourse(string videoId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Quiz quiz = await _quizService.GetQuizForVideoAsync(videoId, userId);

            return Ok(quiz);
        }


        [HttpPost("video/{videoId}"), Authorize]
        public async Task<ActionResult<Quiz>> PostQuizForVideo([FromBody]Quiz quiz)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Quiz savedQuiz = await _quizService.SaveQuizForVideo(quiz, userId);

            return Ok(savedQuiz);
        }

    }

}