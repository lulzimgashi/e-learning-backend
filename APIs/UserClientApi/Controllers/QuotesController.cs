using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.QuoteService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class QuotesController : Controller
    {
        private readonly IQuoteService _quoteService;

        public QuotesController(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }


        [HttpGet]
        public async Task<ActionResult<IList<Quote>>> GetAllQuotes()
        {
            IList<Quote> quotes = await _quoteService.GetAllQuotesAsync();

            return Ok(quotes);
        }



        [HttpPut("save/{quoteId}"), Authorize]
        public async Task<IActionResult> SaveQuote(string quoteId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _quoteService.SaveUserQuoteAsync(userId, quoteId);

            return Ok();
        }

        [HttpPut("unsave/{quoteId}"), Authorize]
        public async Task<IActionResult> UnSaveQuote(string quoteId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _quoteService.UnSaveUserQuoteAsync(userId, quoteId);

            return Ok();
        }


    }

}