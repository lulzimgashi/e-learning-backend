﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.UserService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }


        [HttpPut, Authorize]
        public async Task<ActionResult<User>> UpdateUser([FromBody]User user)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            user.id = userId;

            User updatedUser = await _userService.UpdateUserAsync(user);

            return Ok(updatedUser); ;
        }


        [HttpGet("login"), Authorize]
        public async Task<IActionResult> Login()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            User user = await _userService.GetUserDataAfterLogin(userId);

            return Ok(user);
        }


        [HttpGet("saved-items"), Authorize]
        public async Task<ActionResult<SavedItems>> GetUserSavedItems()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            SavedItems savedItems = await _userService.GetUserSavedItems(userId);

            return Ok(savedItems);
        }



        [HttpPost("register")]
        public async Task<ActionResult<User>> Register([FromBody]User user)
        {
            User registeredUser = await _userService.RegiserUser(user);

            return Ok(registeredUser); ;
        }



        [HttpPut("change-password"), Authorize]
        public async Task<IActionResult> ChangeUserPassword(string newPassword)
        {

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _userService.ChangeUserPassword(newPassword, userId);

            return Ok();
        }



        [HttpPut("change-email"), Authorize]
        public async Task<IActionResult> ChangeUserEmail(string newEmail)
        {

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _userService.ChangeUserEmail(newEmail, userId);

            return Ok();
        }



        [HttpPut("reset-password")]
        public async Task<IActionResult> ResetUserPassword(string email)
        {

            await _userService.ResetUserPassword(email);

            return Ok();
        }


    }
}
