using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.CourseService;
using Services.MessageService;
using Services.QuoteService;
using Services.VideoService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : Controller
    {
        private readonly IMessageService _messageService;

        public MessagesController(IMessageService messageService)
        {
            _messageService = messageService;
        }


        [HttpGet, Authorize]
        public async Task<ActionResult<IList<Message>>> GetUserMessages()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            IList<Message> messages = await _messageService.GetUserMessagesAsync(userId);

            return Ok(messages);
        }


        [HttpPost, Authorize]
        public async Task<ActionResult<Message>> AddNewMessage([FromBody]Message message)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            message.from = userId;


            Message savedMessage = await _messageService.AddNewMessageService(message);

            return Ok(savedMessage);
        }

    }

}