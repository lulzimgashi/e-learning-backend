using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Services.CourseService;
using Services.QuestionService;
using Services.VideoService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class QuestionsController : Controller
    {
        private readonly IQuestionService _questionService;

        public QuestionsController(IQuestionService questionService)
        {
            _questionService = questionService;
        }


        [HttpGet]
        public async Task<ActionResult<IList<Question>>> GetAllQuestions()
        {
            IList<Question> questions = await _questionService.GetAllQuestionsAsync();

            return Ok(questions);
        }


        [HttpGet("{questionId}")]
        public async Task<ActionResult<Question>> GetSingleQuestion(string questionId)
        {
            Question question = await _questionService.GetSingleQuestionAsync(questionId);

            return Ok(question);
        }


        [HttpGet("parent/{parentId}")]
        public async Task<ActionResult<IList<Question>>> GetQuestionsByParentId(string parentId)
        {
            IList<Question> questions = await _questionService.GetQuestionByParentIdAsync(parentId);

            return Ok(questions);
        }


        [HttpPost("channel"), Authorize]
        public async Task<ActionResult<ChannelQuestionPayload>> PostQuestionForChannel([FromBody]ChannelQuestionPayload question)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            question.questionBy = userId;

            ChannelQuestionPayload savedQuestion = await _questionService.PostQuestionForChannelAsync(question);

            return Ok(savedQuestion);
        }


        [HttpPost("video"), Authorize]
        public async Task<ActionResult<ChannelQuestionPayload>> PostQuestionForVideo([FromBody]Question question)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            question.questionBy = userId;

            Question savedQuestion = await _questionService.PostQuestionForVideoAsync(question);

            return Ok(question);
        }



        [HttpPut("save/{questionId}"), Authorize]
        public async Task<IActionResult> SaveQuestion(string questionId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _questionService.SaveUserQuestionAsync(userId, questionId);

            return Ok();
        }

        [HttpPut("unsave/{questionId}"), Authorize]
        public async Task<IActionResult> UnSaveQuestion(string questionId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _questionService.UnSaveUserQuestionAsync(userId, questionId);

            return Ok();
        }


    }

}