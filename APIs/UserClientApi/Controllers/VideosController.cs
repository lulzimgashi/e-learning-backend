using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.VideoService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class VideosController : Controller
    {
        private readonly IVideoService _videoService;

        public VideosController(IVideoService videoService)
        {
            _videoService = videoService;
        }


        [HttpPost, Authorize(Roles = "ADMIN ")]
        public async Task<ActionResult<Video>> CreateVideo([FromBody] Video video)
        {
            Video createVideo = await _videoService.CreateVideoAsync(video);

            return Ok(createVideo);
        }


        [HttpPut, Authorize(Roles = "ADMIN ")]
        public async Task<ActionResult<Video>> UpdateVideo([FromBody] Video video)
        {
            Video updatedVideo = await _videoService.UpdateVideoAsync(video);

            return Ok(updatedVideo);
        }


        [HttpGet("admin"), Authorize(Roles = "ADMIN ")]
        public async Task<ActionResult<IList<Video>>> GetVideosAdminForSingleCourse(string courseId)
        {

            IList<Video> videos = await _videoService.GetVideosAdminForSingleCourseAsync(courseId);

            return Ok(videos);
        }



        [HttpGet("{videoId}")]
        public async Task<ActionResult<Video>> GetSingleVideo(string videoId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Video video = await _videoService.GetSingleVideoAsync(videoId, userId);

            return Ok(video);
        }


        [HttpDelete("{videoId}"), Authorize(Roles = "ADMIN ")]
        public async Task<ActionResult> DeleteVideo(string videoId)
        {
            await _videoService.DeleteVideoAsync(videoId);

            return Ok();
        }


        [HttpGet("live")]
        public async Task<ActionResult<IList<LiveVideo>>> GetLiveVideos()
        {

            IList<LiveVideo> videos = await _videoService.GetLiveVideosAsync();

            return Ok(videos);
        }

        [HttpGet("live/{videoId}")]
        public async Task<ActionResult<LiveVideo>> GetLiveVideo(string videoId)
        {

            LiveVideo video = await _videoService.GetSingleLiveVideoAsync(videoId);

            return Ok(video);
        }


        [HttpPut("save/{videoId}"), Authorize]
        public async Task<IActionResult> SaveVideo(string videoId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _videoService.SaveUserVideoAsync(userId, videoId);

            return Ok();
        }

        [HttpPut("unsave/{videoId}"), Authorize]
        public async Task<IActionResult> UnSaveVideo(string videoId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _videoService.UnSaveUserVideoAsync(userId, videoId);

            return Ok();
        }


        [HttpPost("transcript/{videoId}"), Authorize]
        public async Task<IActionResult> SaveVideoTranscript([FromBody] Transcript transcript, string videoId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            transcript.videoId = videoId;
            transcript.userId = userId;

            Transcript savedTranscript = await _videoService.AddVideoTranscriptAsync(transcript);

            return Ok(savedTranscript);
        }


        [HttpGet("transcript/{videoId}")]
        public async Task<IActionResult> GetVideoTranscript(string videoId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Transcript savedTranscript = await _videoService.GetVideoTranscriptAsync(userId, videoId);

            return Ok(savedTranscript);
        }

    }

}