using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.PublicModels;
using Services.CourseService;

namespace UserClientApi.Controllers
{
    [Route("api/[controller]")]
    public class CoursesController : Controller
    {
        private readonly ICourseService _courseService;

        public CoursesController(ICourseService courseService)
        {
            _courseService = courseService;
        }



        [HttpGet]
        public async Task<ActionResult<IList<Course>>> GetAllCourses()
        {
            IList<Course> courses = await _courseService.GetAllCoursesAsync();

            return Ok(courses);
        }

        [HttpGet("admin"), Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<IList<Course>>> GetAllCoursesAdmin(string channelId)
        {

            IList<Course> courses = await _courseService.GetAllCoursesAdminAsync(channelId);

            return Ok(courses);
        }


        [HttpPost, Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Course>> CreateCourse([FromBody] Course course)
        {
            Course createdCourse = await _courseService.CreateCourseAsync(course);

            return Ok(createdCourse);
        }

        [HttpPut, Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Course>> UpdateCourse([FromBody] Course course)
        {
            Course updatedCourse = await _courseService.UpdateCourseAsync(course);

            return Ok(updatedCourse);
        }


        [HttpGet("kids")]
        public async Task<ActionResult<IList<Course>>> GetAllKidsCourse()
        {
            IList<Course> courses = await _courseService.GetAllKidsCourseAsync();

            return Ok(courses);
        }

        [HttpGet("admin/{courseId}"), Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Course>> GetSingleCourseAdmin(string courseId)
        {
            Course course = await _courseService.GetSingleCourseAdminAsync(courseId);

            return Ok(course);
        }


        [HttpGet("{courseId}")]
        public async Task<ActionResult<Course>> GetSingleCourse(string courseId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Course course = await _courseService.GetSingleCourseAsync(courseId, userId);

            return Ok(course);
        }

        [HttpDelete("{courseId}"), Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Course>> DeleteCourse(string courseId)
        {
            await _courseService.DeleteCourseAsync(courseId);

            return Ok();
        }

        [HttpPut("save/{courseId}"), Authorize]
        public async Task<IActionResult> SaveCourse(string courseId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _courseService.SaveUserCourseAsync(userId, courseId);

            return Ok();
        }

        [HttpPut("unsave/{courseId}"), Authorize]
        public async Task<IActionResult> UnSaveCourse(string courseId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _courseService.UnSaveUserCourseAsync(userId, courseId);

            return Ok();
        }

    }

}