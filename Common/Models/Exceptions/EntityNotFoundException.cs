using System;
using System.Net;

namespace Models.PublicModels.Exceptions
{
    public class EntityNotFoundException : BaseException
    {


        public EntityNotFoundException() : base("Entity not found exception", ExceptionType.EntityNotFoundException, HttpStatusCode.NotFound)
        {
        }


        public EntityNotFoundException(string message, ExceptionType type, HttpStatusCode code) : base(message, type, code)
        {
        }

    }
}
