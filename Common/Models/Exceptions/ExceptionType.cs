using System;

namespace Models.PublicModels.Exceptions
{
    public enum ExceptionType
    {
        ServerError,
        UserAlreadyExistsException,
        EntityNotFoundException
    }
}
