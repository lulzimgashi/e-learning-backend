using System;
using System.Net;

namespace Models.PublicModels.Exceptions
{
    public abstract class BaseException : Exception
    {
        public ExceptionType type { get; set; } = ExceptionType.ServerError;
        public HttpStatusCode code { get; set; } = HttpStatusCode.InternalServerError;


        public BaseException(string message, ExceptionType type, HttpStatusCode code) : base(message)
        {
            this.code = code;
            this.type = type;
        }
    }
}
