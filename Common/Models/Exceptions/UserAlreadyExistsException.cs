using System;
using System.Net;

namespace Models.PublicModels.Exceptions
{
    public class UserAlreadyExistsException : BaseException
    {


        public UserAlreadyExistsException() : base("User is already registered", ExceptionType.UserAlreadyExistsException, HttpStatusCode.BadRequest)
        {
        }


        public UserAlreadyExistsException(string message, ExceptionType type, HttpStatusCode code) : base(message, type, code)
        {
        }

    }
}
