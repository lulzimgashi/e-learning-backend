
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class Course
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public IList<RelatedVideo> videosList { get; set; }
        public IList<string> tagsList { get; set; }
        public string channelId { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public bool active { get; set; }
        public bool saved { get; set; }

        //relations
        public CourseChannel channel { get; set; }
    }


    public class CourseAdmin : Course
    {
        public new IList<Video> videosList { get; set; }

    }

}
