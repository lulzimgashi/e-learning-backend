
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class Quiz
    {
        public string id { get; set; }
        public string name { get; set; }
        public IList<QuizQuestion> questions { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public double score { get; set; }
        public bool completed { get; set; }
        public bool active { get; set; }
        public string videoId { get; set; }
    }
}
