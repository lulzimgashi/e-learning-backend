
using System.Collections.Generic;

namespace Models.PublicModels
{

    public class SavedItems
    {
        public IList<Announcement> announcements { get; set; }
        public IList<Course> courses { get; set; }
        public IList<Question> questions { get; set; }
        public IList<Quote> quotes { get; set; }
        public IList<Video> videos { get; set; }
    }
}
