
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class Quote
    {
        public string id { get; set; }
        public string content { get; set; }
        public IList<string> tagsList { get; set; }
        public string channelId { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public bool active { get; set; }

        //relations
        public CourseChannel channel { get; set; }
    }
}
