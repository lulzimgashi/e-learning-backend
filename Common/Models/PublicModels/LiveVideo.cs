
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class LiveVideo : Video
    {
        public string videoId { get; set; }
        public string image { get; set; }

    }
}
