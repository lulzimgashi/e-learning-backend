
using System.Collections.Generic;

namespace Models.PublicModels
{

    public class Channel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public string websiteUrl { get; set; }
        public string facebookUrl { get; set; }
        public string instagramUrl { get; set; }
        public bool active { get; set; }

        public int totalCourses { get; set; }
        public int totalVideos { get; set; }
        public int totalQuestions { get; set; }

        public IList<Course> coursesList { get; set; }

        public string userId { get; set; }

        public long createdAt { get; set; }
        public long updatedAt { get; set; }
    }
}
