
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class Message
    {
        public string id { get; set; }
        public string message { get; set; }
        public string reply { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
    }

}
