namespace Models.PublicModels
{

    public class UserQuizMap
    {
        public string userId { get; set; }
        public string quizId { get; set; }
        public double score { get; set; }

    }
}
