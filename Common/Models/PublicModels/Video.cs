
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class Video : GenericModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string videoUrl { get; set; }
        public IList<string> tagsList { get; set; }
        public string courseId { get; set; }
        public string channelId { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public bool active { get; set; }
        public bool saved { get; set; }


        //relations
        public IList<RelatedVideo> relatedVideos { get; set; }
        public VideoCourse course { get; set; }
        public CourseChannel channel { get; set; }


    }
}
