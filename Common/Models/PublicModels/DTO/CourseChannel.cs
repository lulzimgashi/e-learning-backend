
using System.Collections.Generic;

namespace Models.PublicModels.DTO
{

    public class CourseChannel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string image { get; set; }

    }
}
