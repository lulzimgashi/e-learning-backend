
using System.Collections.Generic;

namespace Models.PublicModels.DTO
{

    public class RelatedVideo
    {
        public string id { get; set; }
        public string name { get; set; }
        public IList<string> tagsList { get; set; }
    }
}
