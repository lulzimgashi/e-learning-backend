
using System.Collections.Generic;

namespace Models.PublicModels.DTO
{

    public class VideoCourse
    {
        public string id { get; set; }
        public string name { get; set; }

    }
}
