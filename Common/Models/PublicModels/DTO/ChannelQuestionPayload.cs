
using System.Collections.Generic;

namespace Models.PublicModels.DTO
{

    public class ChannelQuestionPayload
    {
        public string id { get; set; }
        public string question { get; set; }
        public string targetChannelId { get; set; }
        public string questionBy { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }

    }
}
