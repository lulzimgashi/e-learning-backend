
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class Question
    {
        public string id { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        public IList<string> tagsList { get; set; }
        public string questionBy { get; set; }
        public string answerBy { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public string videoId { get; set; }
        public bool active { get; set; }

        public CourseChannel channelWhoAnswered { get; set; }
    }

}
