namespace Models.PublicModels
{

    public class Transcript
    {
        public string id { get; set; }
        public string content { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public bool isApproved { get; set; }
        public string userId { get; set; }
        public string videoId { get; set; }

    }
}
