
namespace Models.PublicModels
{
    
    public class User
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public bool isCreator { get; set; }
    }
}
