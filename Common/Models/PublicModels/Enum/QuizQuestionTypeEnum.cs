
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public static class QuizQuestionTypeEnum
    {
        public static readonly string MULTIPLE_OPTIONS = "MULTIPLE_OPTIONS";
        public static readonly string SINGLE_OPTION = "SINGLE_OPTION";
    }
}
