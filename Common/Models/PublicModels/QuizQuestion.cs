
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class QuizQuestion
    {
        public string id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        
        public string optionsJson { get; set; }
        public IList<QuizQuestionOption> options { get; set; }
        public IList<string> answers { get; set; }

        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public string quizId { get; set; }

        public int order { get; set; }
    }
}
