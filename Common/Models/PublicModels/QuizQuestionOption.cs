
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class QuizQuestionOption
    {
        public string id { get; set; }
        public string text { get; set; }

        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        
    }
}
