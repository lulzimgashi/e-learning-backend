
using System.Collections.Generic;
using Models.PublicModels.DTO;

namespace Models.PublicModels
{

    public class Announcement
    {
        public string id { get; set; }
        public string name { get; set; }
        public string content { get; set; }
        public long createdAt { get; set; }
        public long updatedAt { get; set; }
        public string userId { get; set; }
        public bool active { get; set; }
        
    }
}
