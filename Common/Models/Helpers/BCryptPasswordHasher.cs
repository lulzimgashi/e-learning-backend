
using System;

namespace Models.PublicModels
{
    public class BCryptPasswordHasher
    {

        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, 4);
        }

        public bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            if (hashedPassword == null) { throw new ArgumentNullException(nameof(hashedPassword)); }
            if (providedPassword == null) { throw new ArgumentNullException(nameof(providedPassword)); }

            if (BCrypt.Net.BCrypt.Verify(providedPassword, hashedPassword))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }

}