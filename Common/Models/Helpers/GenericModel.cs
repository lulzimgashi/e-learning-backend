
using System.Collections.Generic;

namespace Models.PublicModels
{
    public abstract class GenericModel
    {
        private readonly Dictionary<string, object> _dynamicProperties = new Dictionary<string, object>();

        public object this[string key]
        {
            get
            {
                dynamic value;
                _dynamicProperties.TryGetValue(key, out value);

                return value;
            }
            set
            {
                if (_dynamicProperties.ContainsKey(key))
                {
                    _dynamicProperties[key] = value;
                }
                else
                {
                    _dynamicProperties.Add(key, value);
                }
            }
        }


    }
}
