
namespace Models.PublicModels
{
    public static class ExtensionMethods
    {

        public static User WithoutPassword(this User user)
        {
            user.password = null;
            return user;
        }

        
    }
}
