using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Repositories.Configurations;

namespace Repositories.VideoRepository
{
    public interface IQuestionRepository : IGenericRepository<Question>
    {
        Task<IList<Question>> FindAllQuestionsForParentAsync(string parentId);
        Task<IList<Question>> FindAllQuestions();
        Task<Question> FindSingleQuestionAsync(string questionId);
        Task<int> CountAllQuestionsByChannelId(string channelId);
        Task AddQuestionForChannelAsync(ChannelQuestionPayload question);
        Task AddQuestionForVideoAsync(Question question);
        Task SaveUserQuestionAsync(string userId, string questionId);
        Task UnSaveUserQuestionAsync(string userId, string questionId);
        Task<bool> CheckIfQuetionIsSavedAlready(string userId, string questionId);
        Task<IList<Question>> FindAllSavedByUserId(string userId);
    }

}