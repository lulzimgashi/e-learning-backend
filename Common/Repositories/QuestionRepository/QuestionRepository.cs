using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;
using Models.PublicModels.DTO;

namespace Repositories.VideoRepository
{
    public class QuestionRepository : SqlGenericRepository<Question>, IQuestionRepository
    {
        public QuestionRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }

        public async Task AddQuestionForChannelAsync(ChannelQuestionPayload question)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO questions (id, question, created_at, updated_at,question_by, target_channel_id) VALUES (@id, @question, @createdAt, @updatedAt,@questionBy, @targetChannelId);", question);
            }
        }

        public async Task AddQuestionForVideoAsync(Question question)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO questions (id, question, created_at, updated_at,question_by, video_id) VALUES (@id, @question, @createdAt, @updatedAt,@questionBy, @videoId);", question);
            }
        }


        public async Task<int> CountAllQuestionsByChannelId(string channelId)
        {
            using (var connection = GetOpenConnection())
            {
                var questionsCount = await connection.ExecuteScalarAsync<int>("SELECT COUNT(*) FROM questions WHERE answer_by=@channelId AND active=1", new { channelId });

                return questionsCount;
            }
        }

        public async Task<IList<Question>> FindAllQuestions()
        {
            using (var connection = GetOpenConnection())
            {
                var questions = await connection.QueryAsync<Question, CourseChannel, Question>("SELECT q.*,ch.* FROM questions q INNER JOIN channels ch ON ch.id = q.answer_by WHERE q.active = 1 AND ch.active=1;",
                (question, channelWhoAnswered) =>
                {
                    question.channelWhoAnswered = channelWhoAnswered;

                    return question;
                });

                return questions.AsList();
            }

        }


        public async Task<IList<Question>> FindAllQuestionsForParentAsync(string parentId)
        {
            using (var connection = GetOpenConnection())
            {
                var questions = await connection.QueryAsync<Question, CourseChannel, Question>("SELECT q.*,ch.* FROM questions q INNER JOIN channels ch ON ch.id = q.answer_by WHERE q.video_id =@parentId AND q.active = 1 AND ch.active=1;",
                (question, channelWhoAnswered) =>
                {
                    question.channelWhoAnswered = channelWhoAnswered;

                    return question;
                }, new { parentId });

                return questions.AsList();
            }

        }

        public async Task<Question> FindSingleQuestionAsync(string questionId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedQuestions = await connection.QueryAsync<Question>("SELECT * FROM questions WHERE id=@questionId AND active=1;", new { questionId });


                if (foundedQuestions.AsList().Count > 0)
                {
                    return foundedQuestions.AsList()[0];
                }

                return null;

            }
        }

        public async Task SaveUserQuestionAsync(string userId, string questionId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO saved_questions (user_id,question_id) VALUES (@userId,@questionId);", new { userId, questionId });
            }
        }

        public async Task UnSaveUserQuestionAsync(string userId, string questionId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM saved_questions WHERE user_id=@userId AND question_id=@questionId;", new { userId, questionId });
            }
        }

        public async Task<bool> CheckIfQuetionIsSavedAlready(string userId, string questionId)
        {
            using (var connection = GetOpenConnection())
            {
                return await connection.ExecuteScalarAsync<bool>("SELECT COUNT(*) FROM saved_questions WHERE user_id=@userId AND question_id=@questionId;", new { userId, questionId });
            }
        }

        public async Task<IList<Question>> FindAllSavedByUserId(string userId)
        {
            using (var connection = GetOpenConnection())
            {
                var savedQuestions = await connection.QueryAsync<Question>("SELECT q.* FROM questions q INNER JOIN saved_questions sq ON sq.question_id=q.id WHERE sq.user_id=@userId", new { userId });

                return savedQuestions.AsList();
            }
        }
    }

}