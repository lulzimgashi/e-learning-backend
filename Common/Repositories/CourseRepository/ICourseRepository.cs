using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;

namespace Repositories.CourseRepository
{
    public interface ICourseRepository : IGenericRepository<Course>
    {

        Task<IList<Course>> FindAllCoursesAsync();
        Task<IList<Course>> FindAllKidsCoursesAsync();
        
        Task<IList<Course>> FindAllCoursesByChannelIdAsync(string channelId);

        Task<Course> FindSingleCourseAsync(string courseId);
        Task SaveUserCourseAsync(string userId, string courseId);
        Task UnSaveUserCourseAsync(string userId, string courseId);
        Task<bool> CheckIfUserSavedCourse(string courseId,string loggedUser);
        Task<IList<Course>> FindAllSavedByUserId(string userId);
        Task<IList<Course>> FindAllCoursesForAdminAsync(string channelId);
        Task CreateCourseAsync(Course course);
        Task UpdateCourseAsync(Course course);
        Task DeleteCourseAsync(string courseId);
        Task<Course> FindSingleCourseAdminAsync(string courseId);
    }
}
