using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;
using Models.PublicModels.DTO;

namespace Repositories.CourseRepository
{
    public class CourseRepository : SqlGenericRepository<Course>, ICourseRepository
    {

        public CourseRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }


        public async Task<IList<Course>> FindAllCoursesAsync()
        {
            using (var connection = GetOpenConnection())
            {
                var foundedCourses = await connection.QueryAsync<Course, CourseChannel, Course>("SELECT c.*,ch.id,ch.name FROM courses c INNER JOIN channels ch ON c.channel_id=ch.id WHERE c.active=1 AND ch.active=1",
                   (course, channel) =>
                   {
                       course.channel = channel;

                       return course;
                   });

                return foundedCourses.AsList();
            }
        }


        public async Task<IList<Course>> FindAllCoursesForAdminAsync(string channelId)
        {

            using (var connection = GetOpenConnection())
            {
                var foundedCourses = await connection.QueryAsync<Course>("SELECT c.* FROM courses c WHERE c.channel_id=@channelId", new { channelId });

                return foundedCourses.AsList();

            }
        }


        public async Task<IList<Course>> FindAllKidsCoursesAsync()
        {
            using (var connection = GetOpenConnection())
            {
                var foundedCourses = await connection.QueryAsync<Course, CourseChannel, Course>("SELECT c.*, ch.id, ch.name FROM courses c INNER JOIN channels ch ON c.channel_id = ch.id WHERE 'kids' = ANY (c.tags_list) AND c.active=1 AND ch.active=1;",
                   (course, channel) =>
                   {
                       course.channel = channel;

                       return course;
                   });

                return foundedCourses.AsList();
            }
        }


        public async Task<IList<Course>> FindAllCoursesByChannelIdAsync(string channelId)
        {
            using (var connection = GetOpenConnection())
            {
                var courses = await connection.QueryAsync<Course>("SELECT c.* FROM courses c INNER JOIN channels ch ON c.channel_id=ch.id WHERE c.channel_id=@channelId AND c.active=1 AND ch.active=1;", new { channelId });

                return courses.AsList();
            }
        }


        public async Task<Course> FindSingleCourseAsync(string courseId)
        {
            using (var connection = GetOpenConnection())
            {

                var foundedCourse = await connection.QueryAsync<Course, CourseChannel, Course>("SELECT c.*,ch.id,ch.name FROM courses c INNER JOIN channels ch ON c.channel_id=ch.id WHERE c.id=@id AND c.active=1 AND ch.active=1",
                (course, channel) =>
                {
                    course.channel = channel;

                    return course;
                }
                , new { id = courseId });


                if (foundedCourse.AsList().Count > 0)
                {
                    return foundedCourse.AsList()[0];
                }

                return null;
            }
        }

        public async Task SaveUserCourseAsync(string userId, string courseId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO saved_courses (user_id,course_id) VALUES (@userId,@courseId);", new { userId, courseId });
            }
        }

        public async Task UnSaveUserCourseAsync(string userId, string courseId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM saved_courses WHERE user_id=@userId AND course_id=@courseId;", new { userId, courseId });
            }
        }

        public async Task<bool> CheckIfUserSavedCourse(string courseId, string userId)
        {
            using (var connection = GetOpenConnection())
            {
                return await connection.ExecuteScalarAsync<bool>("SELECT COUNT(*) FROM saved_courses WHERE user_id=@userId AND course_id=@courseId;", new { userId, courseId });
            }
        }

        public async Task<IList<Course>> FindAllSavedByUserId(string userId)
        {
            using (var connection = GetOpenConnection())
            {
                var savedCourses = await connection.QueryAsync<Course>("SELECT c.* FROM courses c INNER JOIN saved_courses sc ON sc.course_id=c.id WHERE sc.user_id=@userId", new { userId });

                return savedCourses.AsList();
            }
        }

        public async Task CreateCourseAsync(Course course)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO courses (id, name, description, tags_list, channel_id, created_at, updated_at, image, active) VALUES (@id, @name, @description,@tagsList, @channelId, @createdAt, @updatedAt,@image,@active::int);", course);
            }
        }

        public async Task UpdateCourseAsync(Course course)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("UPDATE courses SET name=@name,description=@description,image=@image,tags_list=@tagsList,active=@active::int WHERE id=@id;", course);
            }
        }

        public async Task DeleteCourseAsync(string courseId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM courses WHERE id=@courseId", new { courseId });
            }
        }

        public async Task<Course> FindSingleCourseAdminAsync(string courseId)
        {
            Course foundedCourse = null;

            using (var connection = GetOpenConnection())
            {
                var foundedCourses = await connection.QueryAsync<Course, RelatedVideo, Course>("SELECT c.*,v.* FROM courses c LEFT JOIN videos v ON c.id=v.course_id WHERE c.id=@courseId",
                (course, video) =>
                {
                    if (foundedCourse == null)
                    {
                        foundedCourse = course;
                        foundedCourse.videosList = new List<RelatedVideo>();
                    }

                    if (video != null)
                    {
                        foundedCourse.videosList.Add(video);
                    }

                    return foundedCourse;
                }, new { courseId });
            }

            return foundedCourse;
        }
    }
}
