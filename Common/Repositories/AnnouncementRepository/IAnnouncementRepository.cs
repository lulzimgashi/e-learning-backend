using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Repositories.Configurations;

namespace Repositories.AnnouncementRepository
{
    public interface IAnnouncementRepository : IGenericRepository<Announcement>
    {
        Task<IList<Announcement>> FindAllAnnouncementsAsync();
        Task<Announcement> FindSingleAnnouncementAsync(string announcementId);
        Task AddNewAnnouncementAsync(Announcement announcement);
        Task SaveUserAnnouncementAsync(string userId, string announcementId);
        Task UnSaveUserAnnouncementAsync(string userId, string announcementId);
        Task<bool> CheckIfAnnouncmentIsSavedAlready(string userId, string announcementId);
        Task<IList<Announcement>> FindAllByUserId(string userId);
    }

}