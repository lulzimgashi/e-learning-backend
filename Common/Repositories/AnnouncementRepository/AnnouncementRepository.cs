using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;
using Models.PublicModels.DTO;

namespace Repositories.AnnouncementRepository
{
    public class AnnouncementRepository : SqlGenericRepository<Announcement>, IAnnouncementRepository
    {
        public AnnouncementRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }

        public async Task AddNewAnnouncementAsync(Announcement announcement)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO announcements (id,name,content,user_id,created_at,updated_at) VALUES (@id,@name,@content,@userId,@createdAt,@updatedAt)", announcement);
            }
        }


        public async Task<IList<Announcement>> FindAllAnnouncementsAsync()
        {
            using (var connection = GetOpenConnection())
            {
                var foundedCourses = await connection.QueryAsync<Announcement>("SELECT * FROM announcements WHERE active=1");

                return foundedCourses.AsList();
            }
        }

        public async Task<Announcement> FindSingleAnnouncementAsync(string announcementId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedAnnouncements = await connection.QueryAsync<Announcement>("SELECT * FROM announcements WHERE id=@announcementId AND active=1", new { announcementId });


                if (foundedAnnouncements.AsList().Count > 0)
                {
                    return foundedAnnouncements.AsList()[0];
                }

                return null;

            }
        }

        public async Task SaveUserAnnouncementAsync(string userId, string announcementId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO saved_announcements (user_id,announcement_id) VALUES (@userId,@announcementId);", new { userId, announcementId });
            }
        }

        public async Task UnSaveUserAnnouncementAsync(string userId, string announcementId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM saved_announcements WHERE user_id=@userId AND announcement_id=@announcementId;", new { userId, announcementId });
            }
        }

        public async Task<bool> CheckIfAnnouncmentIsSavedAlready(string userId, string announcementId)
        {
            using (var connection = GetOpenConnection())
            {
                return await connection.ExecuteScalarAsync<bool>("SELECT COUNT(*) FROM saved_announcements WHERE user_id=@userId AND announcement_id=@announcementId; ", new { userId, announcementId });
            }
        }

        public async Task<IList<Announcement>> FindAllByUserId(string userId)
        {
            using (var connection = GetOpenConnection())
            {
                var savedAnnouncements = await connection.QueryAsync<Announcement>("SELECT a.* FROM announcements a INNER JOIN saved_announcements sa ON sa.announcement_id=a.id WHERE sa.user_id=@userId", new { userId });

                return savedAnnouncements.AsList();
            }
        }
    }

}