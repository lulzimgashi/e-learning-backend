using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;

namespace Repositories.ChannelRepository
{
    public class ChannelRepository : SqlGenericRepository<Channel>, IChannelRepository
    {

        public ChannelRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }

        public async Task CreateChannelAsync(Channel channel)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO channels (id, name, description, image, website_url, facebook_url, instagram_url, user_id, created_at, updated_at, active) VALUES (@id, @name, @description, @image, @websiteUrl, @facebookUrl, @instagramUrl,@userId, @createdAt, @updatedAt, @active::int);", channel);
            }
        }

        public async Task UpdateChannelAsync(Channel channel)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("UPDATE channels SET name=@name,description=@description,image=@image,website_url=@websiteUrl,facebook_url=@facebookUrl,instagram_url=@instagramUrl,active=@active::int,updated_at=@updatedAt WHERE id=@id;", channel);
            }
        }

        public async Task DeleteChannelAsync(string channelId, string adminId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM channels WHERE id=@channelId AND user_id=@adminId", new { channelId, adminId });
            }
        }

        public async Task<IList<Channel>> FindAllChannelsAdminAsync(string adminId)
        {
            using (var connection = GetOpenConnection())
            {
                var channels = await connection.QueryAsync<Channel>("SELECT * FROM channels ch WHERE  user_id=@adminId", new { adminId });

                return channels.AsList();
            }
        }


        public async Task<IList<Channel>> FindAllChannelsAsync()
        {
            using (var connection = GetOpenConnection())
            {
                var channels = await connection.QueryAsync<Channel>("SELECT * FROM channels WHERE active=1");

                return channels.AsList();
            }
        }

    }
}
