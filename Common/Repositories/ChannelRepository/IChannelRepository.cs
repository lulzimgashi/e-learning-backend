using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;

namespace Repositories.ChannelRepository
{
    public interface IChannelRepository : IGenericRepository<Channel>
    {

        Task<IList<Channel>> FindAllChannelsAsync();
        Task<IList<Channel>> FindAllChannelsAdminAsync(string adminId);
        Task DeleteChannelAsync(string channelId, string adminId);
        Task CreateChannelAsync(Channel channel);
        Task UpdateChannelAsync(Channel channel);
    }
}
