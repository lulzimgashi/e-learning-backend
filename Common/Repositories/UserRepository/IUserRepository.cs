using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;

namespace Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {


        Task<User> FindUserByEmail(string email);

        Task<User> FindUserById(string id);

        Task UpdateUserAsync(User user);

        Task ChangeUserPassword(string id, string newPassword);

        Task ChangeUserEmail(string id, string newEmail);
        
    }
}
