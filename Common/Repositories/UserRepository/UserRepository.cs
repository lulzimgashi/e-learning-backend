using System;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;

namespace Repositories
{

    public class UserRepository : SqlGenericRepository<User>, IUserRepository
    {

        public UserRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {

        }


        public async Task<User> FindUserByEmail(string email)
        {

            using (var connection = GetOpenConnection())
            {
                return await connection.QueryFirstOrDefaultAsync<User>("SELECT * FROM users WHERE email=@email", new { email = email });
            }

        }



        public async Task<User> FindUserById(string id)
        {

            using (var connection = GetOpenConnection())
            {
                return await connection.QueryFirstOrDefaultAsync<User>("SELECT * FROM users WHERE id=@id", new { id = id });
            }

        }


        public async Task ChangeUserPassword(string id, string newPassword)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("UPDATE users SET password=@password WHERE id=@id", new { password = newPassword, id = id });
            }
        }


        public async Task ChangeUserEmail(string id, string newEmail)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("UPDATE users SET email=@email WHERE id=@id", new { email = newEmail, id = id });
            }
        }



        public async Task UpdateUserAsync(User user)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("UPDATE users SET name=@name,updated_at=@updatedAt WHERE id=@id", user);
            }
        }


    }
}
