using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Repositories.Configurations;

namespace Repositories.MessageRepository
{
    public interface IMessageRepository : IGenericRepository<Message>
    {
        Task AddNewMessageForChannelAsync(Message message);
        Task<IList<Message>> FindAllUserMessagesAsync(string userId);
    }

}