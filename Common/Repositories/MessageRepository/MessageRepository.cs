using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;
using Models.PublicModels.DTO;

namespace Repositories.MessageRepository
{
    public class MessageRepository : SqlGenericRepository<Message>, IMessageRepository
    {
        public MessageRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }

        public async Task AddNewMessageForChannelAsync(Message message)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO messages (id,message,\"from\",\"to\",created_at,updated_at) VALUES (@id,@message,@from,@to,@createdAt,@updatedAt);", message);
            }
        }

        public async Task<IList<Message>> FindAllUserMessagesAsync(string userId)
        {
            using (var connection = GetOpenConnection())
            {
                var messages = await connection.QueryAsync<Message>("SELECT * FROM messages WHERE \"from\"=@userId", new { userId });
                return messages.AsList();
            }
        }
    }

}