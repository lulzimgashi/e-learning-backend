using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Repositories.Configurations;

namespace Repositories.VideoRepository
{
    public interface IVideoRepository : IGenericRepository<Video>
    {

        Task<IList<RelatedVideo>> FindAllVideosByCourseId(string courseId);

        Task<Video> FindSingleVideoAsync(string videoId);
        Task<int> CountAllVideosByChannelId(string channelId);

        Task SaveUserVideoAsync(string userId, string videoId);
        Task UnSaveUserVideoAsync(string userId, string videoId);
        Task<bool> CheckIfUserSavedVideo(string videoId, string loggedUser);
        Task AddVideoTranscriptAsync(Transcript transcript);
        Task<Transcript> FindVideoTranscript(string videoId);
        Task<Transcript> FindVideoTranscriptForUser(string userId, string videoId);
        Task<IList<Video>> FindAllSavedByUserId(string userId);
        Task<IList<LiveVideo>> FindAllLiveVideos();
        Task<LiveVideo> FindSingleLiveVideoAsync(string videoId);
        Task AddVideoAsync(Video video);
        Task UpdateVideoAsync(Video video);
        Task deleteVideoAsync(string videoId);
        Task<IList<Video>> FindAllVideosAdminForSingleCourseAsync(string courseId);
    }

}