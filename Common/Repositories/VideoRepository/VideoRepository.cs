using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;
using Models.PublicModels.DTO;

namespace Repositories.VideoRepository
{
    public class VideoRepository : SqlGenericRepository<Video>, IVideoRepository
    {
        public VideoRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }

        public async Task<int> CountAllVideosByChannelId(string channelId)
        {
            using (var connection = GetOpenConnection())
            {
                var videosCount = await connection.ExecuteScalarAsync<int>("SELECT COUNT(*) FROM videos WHERE channel_id=@channelId AND active=1", new { channelId });

                return videosCount;
            }
        }


        public async Task<IList<RelatedVideo>> FindAllVideosByCourseId(string courseId)
        {
            using (var connection = GetOpenConnection())
            {
                var videos = await connection.QueryAsync<RelatedVideo>("SELECT * FROM videos WHERE course_id=@courseId AND active=1", new { courseId });

                return videos.AsList();

            }
        }


        public async Task<Video> FindSingleVideoAsync(string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedVideo = await connection.QueryAsync<Video, VideoCourse, CourseChannel, Video>("SELECT * FROM videos v INNER JOIN courses c ON v.course_id = c.id INNER JOIN channels c2 on v.channel_id = c2.id WHERE v.id =@videoId AND v.active=1 AND c.active=1 AND c2.active=1;",
                (video, videoCourse, courseChannel) =>
                {
                    video.course = videoCourse;
                    video.channel = courseChannel;

                    return video;
                }, new { videoId });


                if (foundedVideo.AsList().Count > 0)
                {
                    return foundedVideo.AsList()[0];
                }

                return null;
            }
        }


        public async Task SaveUserVideoAsync(string userId, string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO saved_videos (user_id,video_id) VALUES (@userId,@videoId);", new { userId, videoId });
            }
        }

        public async Task UnSaveUserVideoAsync(string userId, string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM saved_videos WHERE user_id=@userId AND video_id=@videoId;", new { userId, videoId });
            }
        }

        public async Task<bool> CheckIfUserSavedVideo(string videoId, string userId)
        {
            using (var connection = GetOpenConnection())
            {
                return await connection.ExecuteScalarAsync<bool>("SELECT COUNT(*) FROM saved_videos WHERE user_id=@userId AND video_id=@videoId;", new { userId, videoId });
            }
        }

        public async Task AddVideoTranscriptAsync(Transcript transcript)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO transcripts (id,content,is_approved,user_id,video_id,created_at,updated_at) VALUES (@id,@content,@isApproved::int,@userId,@videoId,@createdAt,@updatedAt)", transcript);
            }
        }

        public async Task<Transcript> FindVideoTranscript(string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedTranscript = await connection.QueryAsync<Transcript>("SELECT * FROM transcripts WHERE video_id=@videoId AND is_approved=1 LIMIT 1", new { videoId });

                if (foundedTranscript.AsList().Count > 0)
                {
                    return foundedTranscript.AsList()[0];
                }

                return null;
            }
        }

        public async Task<Transcript> FindVideoTranscriptForUser(string userId, string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedTranscript = await connection.QueryAsync<Transcript>("SELECT * FROM transcripts WHERE user_id=@userId AND video_id=@videoId LIMIT 1", new { userId, videoId });

                if (foundedTranscript.AsList().Count > 0)
                {
                    return foundedTranscript.AsList()[0];
                }

                return null;
            }
        }

        public async Task<IList<Video>> FindAllSavedByUserId(string userId)
        {
            using (var connection = GetOpenConnection())
            {
                var savedVideos = await connection.QueryAsync<Video>("SELECT v.* FROM videos v INNER JOIN saved_videos sv ON sv.video_id=v.id WHERE sv.user_id=@userId", new { userId });

                return savedVideos.AsList();
            }
        }

        public async Task<IList<LiveVideo>> FindAllLiveVideos()
        {
            using (var connection = GetOpenConnection())
            {
                var liveVideos = await connection.QueryAsync<LiveVideo, CourseChannel, LiveVideo>("SELECT v.*,lv.*,ch.* FROM videos v INNER JOIN live_videos lv ON lv.video_id=v.id INNER JOIN channels ch on v.channel_id = ch.id WHERE v.active=1 AND ch.active=1",
                (liveVideo, courseChannel) =>
                {
                    liveVideo.channel = courseChannel;

                    return liveVideo;
                });

                return liveVideos.AsList();
            }
        }

        public async Task<LiveVideo> FindSingleLiveVideoAsync(string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedVideo = await connection.QueryAsync<LiveVideo, CourseChannel, LiveVideo>("SELECT lv.*, v.*,c.* FROM live_videos lv INNER JOIN videos v on lv.video_id = v.id INNER JOIN channels c on v.channel_id = c.id WHERE lv.video_id=@videoId AND v.active=1 AND c.active=1;",
                (liveVideo, courseChannel) =>
                {
                    liveVideo.channel = courseChannel;

                    return liveVideo;
                }, new { videoId });


                if (foundedVideo.AsList().Count > 0)
                {
                    return foundedVideo.AsList()[0];
                }

                return null;
            }
        }

        public async Task AddVideoAsync(Video video)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO videos (id, name, description, video_url, tags_list, course_id, created_at, updated_at, channel_id, active) VALUES (@id, @name, @description, @videoUrl, @tagsList, @courseId, @createdAt, @updatedAt, @channelId, @active::int);", video);
            }
        }

        public async Task UpdateVideoAsync(Video video)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("UPDATE videos SET name=@name,description=@description,video_url=@videoUrl,tags_list=@tagsList,updated_at=@updatedAt,active=@active::int WHERE id=@id;", video);
            }
        }

        public async Task deleteVideoAsync(string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM videos WHERE id=@videoId", new { videoId });
            }
        }

        public async Task<IList<Video>> FindAllVideosAdminForSingleCourseAsync(string courseId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedCourses = await connection.QueryAsync<Video>("SELECT * FROM videos WHERE course_id=@courseId", new { courseId });

                return foundedCourses.AsList();

            }
        }
    }

}