using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Repositories.Configurations
{
    public interface IGenericRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> FindAllAsync();
        Task<TEntity> FindAsync(string id);
        Task InsertAsync(TEntity entity);
        Task DeleteAsync(int id);
        Task UpdateAsync(TEntity entityToUpdate);
        
    }
}
