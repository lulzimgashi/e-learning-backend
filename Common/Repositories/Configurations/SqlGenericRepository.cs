using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Npgsql;
using Dapper;

namespace Repositories.Configurations
{
    public abstract class SqlGenericRepository<TEntity> : IGenericRepository<TEntity>
    {

        private readonly string _tableName;
        protected string _connectionString;

        protected SqlGenericRepository(string tableName, string connectionString)
        {
            _tableName = tableName;
            _connectionString = connectionString;
        }


        protected IDbConnection GetOpenConnection()
        {
            return new NpgsqlConnection(_connectionString);
        }



        // Generic Method Implementations
        public async Task<IEnumerable<TEntity>> FindAllAsync()
        {
            using (var connection = GetOpenConnection())
            {
                return await connection.QueryAsync<TEntity>($"SELECT * FROM {_tableName}");
            }
        }


        public async Task<TEntity> FindAsync(string id)
        {
            using (var connection = GetOpenConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<TEntity>($"SELECT * FROM {_tableName} WHERE Id=@Id", new { Id = id });

                return result;
            }
        }
        

        public async Task InsertAsync(TEntity entity)
        {
            var insertQuery = SqlGenericRepositoryHelper.GenerateInsertQuery(_tableName, entity);

            System.Console.WriteLine(insertQuery);
            
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync(insertQuery, entity);
            }
        }


        public async Task UpdateAsync(TEntity entityToUpdate)
        {
            var updateQuery = SqlGenericRepositoryHelper.GenerateUpdateQuery(_tableName, entityToUpdate);

            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync(updateQuery, entityToUpdate);
            }
        }
        

        public async Task DeleteAsync(int id)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync($"DELETE FROM {_tableName} WHERE Id=@Id", new { Id = id });
            }
        }

    }
}
