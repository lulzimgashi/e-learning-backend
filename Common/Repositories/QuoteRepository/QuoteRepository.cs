using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;
using Models.PublicModels.DTO;

namespace Repositories.QuoteRepository
{
    public class QuoteRepository : SqlGenericRepository<Quote>, IQuoteRepository
    {
        public QuoteRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }



        public async Task<IList<Quote>> FindAllQuotesAsync()
        {
            using (var connection = GetOpenConnection())
            {
                var quotes = await connection.QueryAsync<Quote, CourseChannel, Quote>("SELECT q.*,ch.* FROM quotes q INNER JOIN channels ch ON ch.id = q.channel_id WHERE q.active = 1 AND ch.active=1;",
                (quote, channel) =>
                {
                    quote.channel = channel;

                    return quote;
                });

                return quotes.AsList();
            }

        }

        public async Task SaveUserQuoteAsync(string userId, string quoteId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO saved_quotes (user_id,quote_id) VALUES (@userId,@quoteId);", new { userId, quoteId });
            }
        }

        public async Task UnSaveUserQuoteAsync(string userId, string quoteId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("DELETE FROM saved_quotes WHERE user_id=@userId AND quote_id=@quoteId;", new { userId, quoteId });
            }
        }


        public async Task<bool> CheckIfQuoteIsSavedAlready(string userId, string quoteId)
        {
            using (var connection = GetOpenConnection())
            {
                return await connection.ExecuteScalarAsync<bool>("SELECT COUNT(*) FROM saved_quotes WHERE user_id=@userId AND quote_id=@quoteId;", new { userId, quoteId });
            }
        }

        public async Task<IList<Quote>> FindAllSavedByUserId(string userId)
        {
            using (var connection = GetOpenConnection())
            {
                var savedQuotes = await connection.QueryAsync<Quote>("SELECT q.* FROM quotes q INNER JOIN saved_quotes sq ON sq.quote_id=q.id WHERE sq.user_id=@userId", new { userId });

                return savedQuotes.AsList();
            }
        }
    }

}