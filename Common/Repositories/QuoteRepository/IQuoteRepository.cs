using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Repositories.Configurations;

namespace Repositories.QuoteRepository
{
    public interface IQuoteRepository : IGenericRepository<Quote>
    {
        Task<IList<Quote>> FindAllQuotesAsync();
        Task SaveUserQuoteAsync(string userId, string quoteId);
        Task UnSaveUserQuoteAsync(string userId, string quoteId);
        Task<bool> CheckIfQuoteIsSavedAlready(string userId, string quoteId);
        Task<IList<Quote>> FindAllSavedByUserId(string userId);
    }

}