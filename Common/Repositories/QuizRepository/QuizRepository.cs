using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories.Configurations;
using Dapper;
using Models.PublicModels.DTO;

namespace Repositories.QuizRepository
{
    public class QuizRepository : SqlGenericRepository<Quiz>, IQuizRepository
    {
        public QuizRepository(string tableName, string connectionString) : base(tableName, connectionString)
        {
        }

        public async Task<UserQuizMap> FindIfUserAlreadyCompletedQuizAsync(string userId, string videoId)
        {
            using (var connection = GetOpenConnection())
            {
                var foundedQuiz = await connection.QueryAsync<UserQuizMap>("SELECT * FROM quiz_results qr INNER JOIN quizes q ON (q.id = qr.quiz_id AND q.video_id =@videoId) WHERE qr.user_id=@userId;", new { userId, videoId });


                if (foundedQuiz.AsList().Count > 0)
                {
                    return foundedQuiz.AsList()[0];
                }

                return null;

            }
        }

        public async Task<Quiz> FindSingleQuizForParentAsync(string parentId)
        {
            Quiz finalQuiz = new Quiz();

            using (var connection = GetOpenConnection())
            {
                var foundedQuiz = await connection.QueryAsync<Quiz, QuizQuestion, Quiz>("SELECT q.*,qq.* FROM quizes q LEFT JOIN quiz_questions qq ON q.id=qq.quiz_id WHERE q.video_id=@parentId ORDER BY qq.\"order\" ",
                (quiz, quizQuestion) =>
                {
                    if (string.IsNullOrWhiteSpace(finalQuiz.id))
                    {
                        finalQuiz = quiz;
                        finalQuiz.questions = new List<QuizQuestion>();
                    }


                    if (quizQuestion != null)
                    {
                        finalQuiz.questions.Add(quizQuestion);
                    }

                    return finalQuiz;
                }, new { parentId = parentId });

            }

            return finalQuiz;

        }

        public async Task SaveQuizScoreAsync(Quiz newQuiz, string userId)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("INSERT INTO quiz_results (user_id,quiz_id,score,created_at,updated_at) VALUES (@userId,@quizId,@score,@createdAt,@updatedAt) ", new { quizId = newQuiz.id, score = newQuiz.score, createdAt = newQuiz.createdAt, updatedAt = newQuiz.updatedAt, userId = userId });
            }
        }
    }

}