using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Repositories.Configurations;

namespace Repositories.QuizRepository
{
    public interface IQuizRepository : IGenericRepository<Quiz>
    {
        Task<Quiz> FindSingleQuizForParentAsync(string parentId);
        Task SaveQuizScoreAsync(Quiz newQuiz, string userId);
        Task<UserQuizMap> FindIfUserAlreadyCompletedQuizAsync(string userId, string videoId);
    }

}