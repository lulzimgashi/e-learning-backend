using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.Exceptions;
using Repositories.ChannelRepository;
using Repositories.CourseRepository;
using Repositories.VideoRepository;
using Services.CourseService;

namespace Services.ChannelService
{
    public class ChannelService : IChannelService
    {
        private readonly IChannelRepository _channelRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IVideoRepository _videoRepository;
        private readonly IQuestionRepository _questionRepository;

        public ChannelService(IChannelRepository channelRepository, ICourseRepository courseRepository, IVideoRepository videoRepository, IQuestionRepository questionRepository)
        {
            _channelRepository = channelRepository;
            _courseRepository = courseRepository;
            _videoRepository = videoRepository;
            _questionRepository = questionRepository;
        }


        public async Task<IList<Channel>> GetAllChannelsAsync()
        {
            IList<Channel> channels = await _channelRepository.FindAllChannelsAsync();

            return channels;
        }

        public async Task<IList<Channel>> GetAllChannelsAdminAsync(string adminId)
        {
            IList<Channel> channels = await _channelRepository.FindAllChannelsAdminAsync(adminId);

            return channels;
        }


        public async Task<Channel> GetSingleChannelAsync(string channelId)
        {
            Channel channel = await _channelRepository.FindAsync(channelId);
            if (channel == null)
            {
                throw new EntityNotFoundException();
            }

            IList<Course> courses = await _courseRepository.FindAllCoursesByChannelIdAsync(channelId);
            channel.coursesList = courses;

            int videosCount = await _videoRepository.CountAllVideosByChannelId(channelId);
            channel.totalVideos = videosCount;

            int questionsCount = await _questionRepository.CountAllQuestionsByChannelId(channelId);
            channel.totalQuestions = questionsCount;


            channel.totalCourses = courses.Count;

            return channel;
        }

        public async Task DeleteChannelAsync(string channelId, string adminId)
        {
            await _channelRepository.DeleteChannelAsync(channelId, adminId);
        }

        public async Task<Channel> CreateChannelAsync(Channel channel)
        {
            channel.id = System.Guid.NewGuid().ToString();
            channel.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            channel.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _channelRepository.CreateChannelAsync(channel);

            return channel;
        }

        public async Task<Channel> UpdateChannelAsync(Channel channel)
        {
            channel.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            await _channelRepository.UpdateChannelAsync(channel);

            return channel;
        }
    }
}
