using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;

namespace Services.ChannelService
{
    public interface IChannelService
    {

        Task<IList<Channel>> GetAllChannelsAsync();

        Task<Channel> GetSingleChannelAsync(string channelId);
        Task<IList<Channel>> GetAllChannelsAdminAsync(string adminId);
        Task DeleteChannelAsync(string channelId, string adminId);
        Task<Channel> CreateChannelAsync(Channel channel);
        Task<Channel> UpdateChannelAsync(Channel channel);
    }
}
