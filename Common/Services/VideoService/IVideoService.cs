using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;

namespace Services.VideoService
{
    public interface IVideoService
    {

        Task<Video> GetSingleVideoAsync(string videoId,string loggedUser);
        Task SaveUserVideoAsync(string userId, string videoId);
        Task UnSaveUserVideoAsync(string userId, string videoId);
        Task<Transcript> AddVideoTranscriptAsync(Transcript transcript);
        Task<Transcript> GetVideoTranscriptAsync(string userId, string videoId);
        Task<IList<LiveVideo>> GetLiveVideosAsync();
        Task<LiveVideo> GetSingleLiveVideoAsync(string videoId);
        Task<Video> CreateVideoAsync(Video video);
        Task DeleteVideoAsync(string videoId);
        Task<Video> UpdateVideoAsync(Video video);
        Task<IList<Video>> GetVideosAdminForSingleCourseAsync(string courseId);
    }

}
