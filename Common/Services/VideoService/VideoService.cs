using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Models.PublicModels.Exceptions;
using Repositories.VideoRepository;

namespace Services.VideoService
{
    public class VideoService : IVideoService
    {
        private readonly IVideoRepository _videoRepository;

        public VideoService(IVideoRepository videoRepository)
        {
            _videoRepository = videoRepository;
        }


        public async Task<Video> GetSingleVideoAsync(string videoId, string loggedUser)
        {
            Video video = await _videoRepository.FindSingleVideoAsync(videoId);
            if (video == null)
            {
                throw new EntityNotFoundException();
            }


            //get videos of course
            IList<RelatedVideo> videos = await _videoRepository.FindAllVideosByCourseId(video.courseId);
            video.relatedVideos = videos;

            //check if user saved video already
            if (!string.IsNullOrWhiteSpace(loggedUser))
            {
                video.saved = await _videoRepository.CheckIfUserSavedVideo(video.id, loggedUser);
            }


            return video;
        }


        public async Task SaveUserVideoAsync(string userId, string videoId)
        {
            bool savedAlready = await _videoRepository.CheckIfUserSavedVideo(videoId, userId);
            if (!savedAlready)
            {
                await _videoRepository.SaveUserVideoAsync(userId, videoId);
            }
        }

        public async Task UnSaveUserVideoAsync(string userId, string videoId)
        {
            await _videoRepository.UnSaveUserVideoAsync(userId, videoId);
        }


        public async Task<Transcript> AddVideoTranscriptAsync(Transcript transcript)
        {
            transcript.id = System.Guid.NewGuid().ToString();
            transcript.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            transcript.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            transcript.isApproved = false;

            await _videoRepository.AddVideoTranscriptAsync(transcript);

            return transcript;
        }

        public async Task<Transcript> GetVideoTranscriptAsync(string userId, string videoId)
        {
            Transcript transcript = await _videoRepository.FindVideoTranscript(videoId);
            if (transcript != null && !string.IsNullOrWhiteSpace(transcript.id))
            {
                return transcript;
            }

            if (!string.IsNullOrWhiteSpace(userId))
            {
                transcript = await _videoRepository.FindVideoTranscriptForUser(userId, videoId);
                if (transcript != null && !string.IsNullOrWhiteSpace(transcript.id))
                {
                    return transcript;
                }
            }

            throw new EntityNotFoundException();
        }

        public async Task<IList<LiveVideo>> GetLiveVideosAsync()
        {
            IList<LiveVideo> liveVideos = await _videoRepository.FindAllLiveVideos();

            return liveVideos;
        }

        public async Task<LiveVideo> GetSingleLiveVideoAsync(string videoId)
        {
            LiveVideo liveVideo = await _videoRepository.FindSingleLiveVideoAsync(videoId);
            if (liveVideo == null)
            {
                throw new EntityNotFoundException();
            }

            return liveVideo;
        }

        public async Task<Video> CreateVideoAsync(Video video)
        {
            video.id = System.Guid.NewGuid().ToString();
            video.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            video.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _videoRepository.AddVideoAsync(video);

            return video;
        }


        public async Task<Video> UpdateVideoAsync(Video video)
        {
            video.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _videoRepository.UpdateVideoAsync(video);

            return video;
        }

        public async Task DeleteVideoAsync(string videoId)
        {
            await _videoRepository.deleteVideoAsync(videoId);
        }

        public async Task<IList<Video>> GetVideosAdminForSingleCourseAsync(string courseId)
        {
            return await _videoRepository.FindAllVideosAdminForSingleCourseAsync(courseId);
        }
    }
}
