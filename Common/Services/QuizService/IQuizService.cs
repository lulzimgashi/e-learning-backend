using System;
using System.Threading.Tasks;
using Models.PublicModels;

namespace Services.QuizService
{
    public interface IQuizService
    {
        Task<Quiz> GetQuizForVideoAsync(string videoId,string userId,bool hideAnswers = true);
        Task<Quiz> SaveQuizForVideo(Quiz quiz, string userId);
    }

}
