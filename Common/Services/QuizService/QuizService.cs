using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Models.PublicModels.Exceptions;
using Newtonsoft.Json;
using Repositories.QuizRepository;
using Repositories.VideoRepository;

namespace Services.QuizService
{
    public class QuizService : IQuizService
    {
        private readonly IQuizRepository _quizRepository;

        public QuizService(IQuizRepository quizRepository)
        {
            _quizRepository = quizRepository;
        }


        public async Task<Quiz> GetQuizForVideoAsync(string videoId, string userId, bool hideAnswers = true)
        {
        
            UserQuizMap userQuiz = await _quizRepository.FindIfUserAlreadyCompletedQuizAsync(userId, videoId);
            if (userQuiz != null && !string.IsNullOrWhiteSpace(userQuiz.userId))
            {
                Quiz newQuiz = new Quiz();
                newQuiz.id = userQuiz.quizId;
                newQuiz.score = userQuiz.score;
                newQuiz.completed=true;
                return newQuiz;
            }



            Quiz quiz = await _quizRepository.FindSingleQuizForParentAsync(videoId);
            if (quiz == null || string.IsNullOrWhiteSpace(quiz.id))
            {
                throw new EntityNotFoundException();
            }

            if (quiz.questions == null)
            {
                return quiz;
            }
            foreach (QuizQuestion question in quiz.questions)
            {
                question.options = JsonConvert.DeserializeObject<IList<QuizQuestionOption>>(question.optionsJson);
                question.optionsJson = null;
                if (hideAnswers)
                {
                    question.answers = null;
                }
            }

            return quiz;
        }

        public async Task<Quiz> SaveQuizForVideo(Quiz quiz, string userId)
        {
            Quiz dbQuiz = await GetQuizForVideoAsync(quiz.videoId, userId, false);
            if (quiz == null || string.IsNullOrWhiteSpace(quiz.id))
            {
                throw new EntityNotFoundException();
            }


            double scorePerQuestion = 100.0 / dbQuiz.questions.Count;
            double totalScore = 0;


            foreach (QuizQuestion dbQuizQuestion in dbQuiz.questions)
            {

                var correctAnswers = dbQuizQuestion.answers;
                var userAnswers = quiz.questions.Where((quizQuestion) => quizQuestion.id == dbQuizQuestion.id).ToList()[0].answers;

                if (dbQuizQuestion.type == QuizQuestionTypeEnum.SINGLE_OPTION)
                {
                    if ((correctAnswers.Count == userAnswers.Count) && (correctAnswers[0] == userAnswers[0]))
                    {
                        totalScore += scorePerQuestion;
                    }
                }
                else if (dbQuizQuestion.type == QuizQuestionTypeEnum.MULTIPLE_OPTIONS)
                {
                    double scorePerOption = scorePerQuestion / correctAnswers.Count;
                    double totalScorePerOptions = 0;


                    correctAnswers.ToList().ForEach((correctAnswer) =>
                    {
                        if (userAnswers.Contains(correctAnswer))
                        {
                            totalScorePerOptions += scorePerOption;
                        }
                    });


                    userAnswers.ToList().ForEach((userAnswer) =>
                    {
                        if (!correctAnswers.Contains(userAnswer))
                        {
                            totalScorePerOptions -= scorePerOption;
                        }
                    });

                    totalScorePerOptions = totalScorePerOptions > scorePerQuestion ? scorePerQuestion : totalScorePerOptions;
                    totalScorePerOptions = totalScorePerOptions < 0 ? 0 : totalScorePerOptions;
                    totalScore += totalScorePerOptions;

                }

            }


            Quiz newQuiz = new Quiz();
            newQuiz.id = quiz.id;
            newQuiz.completed = true;
            newQuiz.score = Math.Round(totalScore, 2);
            newQuiz.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            newQuiz.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();


            await _quizRepository.SaveQuizScoreAsync(newQuiz, userId);

            return newQuiz;
        }
    }
}
