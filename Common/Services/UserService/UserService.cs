using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Models.PublicModels;
using Models.PublicModels.Exceptions;
using Repositories;
using Repositories.AnnouncementRepository;
using Repositories.CourseRepository;
using Repositories.QuoteRepository;
using Repositories.VideoRepository;

namespace Services.UserService
{

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAnnouncementRepository _announcementRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IQuoteRepository _quoteRepository;
        private readonly IVideoRepository _videoRepository;

        public BCryptPasswordHasher _bCryptPasswordHasher { get; }


        public UserService(
            IUserRepository userRepository,
            IAnnouncementRepository announcementRepository,
            ICourseRepository courseRepository,
            IQuestionRepository questionRepository,
            IQuoteRepository quoteRepository,
            IVideoRepository videoRepository,
            BCryptPasswordHasher bCryptPasswordHasher)
        {
            _userRepository = userRepository;
            _announcementRepository = announcementRepository;
            _courseRepository = courseRepository;
            _questionRepository = questionRepository;
            _quoteRepository = quoteRepository;
            _videoRepository = videoRepository;

            _bCryptPasswordHasher = bCryptPasswordHasher;
        }



        public async Task<User> AuthenticateUser(string email, string password)
        {
            //check if user is founded
            User foundedUser = await _userRepository.FindUserByEmail(email);
            if (foundedUser == null)
            {
                return null;
            }


            //check if password is correct
            bool isPasswordCorrect = _bCryptPasswordHasher.VerifyHashedPassword(foundedUser.password, password);
            if (!isPasswordCorrect)
            {
                return null;
            }


            //remove password
            foundedUser.WithoutPassword();
            return foundedUser;
        }



        public async Task<User> UpdateUserAsync(User user)
        {
            user.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            await _userRepository.UpdateUserAsync(user);

            return user;
        }


        public async Task<User> GetUserDataAfterLogin(string userId)
        {
            User foundedUser = await _userRepository.FindUserById(userId);
            if (foundedUser == null)
            {
                return null;
            }


            foundedUser.WithoutPassword();

            return foundedUser;
        }



        public async Task<User> RegiserUser(User user)
        {

            if (await CheckIfEmailIsRegistered(user.email))
            {
                throw new UserAlreadyExistsException();
            }


            user.id = System.Guid.NewGuid().ToString();
            user.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            user.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            user.password = _bCryptPasswordHasher.HashPassword(user.password);

            await _userRepository.InsertAsync(user);

            user.WithoutPassword();

            return user;
        }


        public async Task ChangeUserPassword(string newPassword, string userId)
        {
            string hashedPassword = _bCryptPasswordHasher.HashPassword(newPassword);
            await _userRepository.ChangeUserPassword(userId, hashedPassword);
        }


        public async Task ChangeUserEmail(string newEmail, string userId)
        {
            bool isAlreadyRegistered = await CheckIfEmailIsRegistered(newEmail);
            if (isAlreadyRegistered)
            {
                throw new UserAlreadyExistsException();
            }


            await _userRepository.ChangeUserEmail(userId, newEmail);

        }

        public async Task ResetUserPassword(string email)
        {

            User foundedUser = await _userRepository.FindUserByEmail(email);
            if (foundedUser == null)
            {
                return;
            }



            string generatedPassword = PasswordGenerator.GeneratePassword(8);
            await ChangeUserPassword(generatedPassword, foundedUser.id);

            System.Console.WriteLine("Generated Password : " + generatedPassword);

            //TODO : send email through SES 

            String source = "gshlulzim@gmail.com"; //From Email
            String recipient = email;
            String awsAccessKey = "key"; 
            String awsSecretKey = "secret"; 

            var oDestination = new Destination(new List<string>() { recipient });
            // Create the email subject.
            var oSubject = new Content("Password Changed");
            // Create the email body.
            var oBody = new Body();
            oBody.Html = new Content($"Your new password is : {generatedPassword}");
            // Create and transmit the email to the recipients via Amazon SES.
            var oMessage = new Amazon.SimpleEmail.Model.Message(oSubject, oBody);
            var request = new SendEmailRequest(source, oDestination, oMessage);
            using (var client = new AmazonSimpleEmailServiceClient(awsAccessKey, awsSecretKey, RegionEndpoint.USEast1))
            {
                await client.SendEmailAsync(request);
            }



        }


        //private methods
        private async Task<bool> CheckIfEmailIsRegistered(string email)
        {

            User foundedUser = await _userRepository.FindUserByEmail(email);

            if (foundedUser != null)
            {
                return true;
            }

            return false;
        }

        public async Task<SavedItems> GetUserSavedItems(string userId)
        {
            IList<Announcement> announcements = await _announcementRepository.FindAllByUserId(userId);
            IList<Course> courses = await _courseRepository.FindAllSavedByUserId(userId);
            IList<Question> questions = await _questionRepository.FindAllSavedByUserId(userId);
            IList<Quote> quotes = await _quoteRepository.FindAllSavedByUserId(userId);
            IList<Video> videos = await _videoRepository.FindAllSavedByUserId(userId);

            SavedItems savedItems = new SavedItems();
            savedItems.announcements = announcements;
            savedItems.courses = courses;
            savedItems.questions = questions;
            savedItems.quotes = quotes;
            savedItems.videos = videos;


            return savedItems;
        }
    }
}
