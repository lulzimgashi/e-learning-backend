using System.Threading.Tasks;
using Models.PublicModels;

namespace Services.UserService
{
    public interface IUserService
    {

        Task<User> AuthenticateUser(string username, string password);

        Task<User> UpdateUserAsync(User user);

        Task<User> GetUserDataAfterLogin(string userId);

        Task<User> RegiserUser(User user);

        Task ChangeUserPassword(string newPassword, string userId);

        Task ChangeUserEmail(string newEmail, string userId);

        Task ResetUserPassword(string email);
        Task<SavedItems> GetUserSavedItems(string userId);
    }
}
