using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;

namespace Services.MessageService
{
    public interface IMessageService
    {
        Task<Message> AddNewMessageService(Message message);
        Task<IList<Message>> GetUserMessagesAsync(string userId);
    }

}
