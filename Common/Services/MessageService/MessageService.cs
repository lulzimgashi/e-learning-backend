using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Models.PublicModels.Exceptions;
using Repositories.MessageRepository;
using Repositories.QuoteRepository;

namespace Services.MessageService
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;

        public MessageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public async Task<Message> AddNewMessageService(Message message)
        {
            message.id = System.Guid.NewGuid().ToString();
            message.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            message.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();


            await _messageRepository.AddNewMessageForChannelAsync(message);

            return message;
        }

        public async Task<IList<Message>> GetUserMessagesAsync(string userId)
        {
            IList<Message> messages = await _messageRepository.FindAllUserMessagesAsync(userId);

            return messages;
        }
    }
}
