using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Models.PublicModels.Exceptions;
using Repositories.VideoRepository;

namespace Services.QuestionService
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;

        public QuestionService(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }


        public async Task<IList<Question>> GetAllQuestionsAsync()
        {
            IList<Question> questions = await _questionRepository.FindAllQuestions();

            return questions;
        }


        public async Task<IList<Question>> GetQuestionByParentIdAsync(string parentId)
        {
            IList<Question> questions = await _questionRepository.FindAllQuestionsForParentAsync(parentId);

            return questions;
        }

        public async Task<Question> GetSingleQuestionAsync(string questionId)
        {
            Question question = await _questionRepository.FindSingleQuestionAsync(questionId);
            if (question == null)
            {
                throw new EntityNotFoundException();
            }

            return question;
        }

        public async Task<ChannelQuestionPayload> PostQuestionForChannelAsync(ChannelQuestionPayload question)
        {
            question.id = System.Guid.NewGuid().ToString();
            question.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            question.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _questionRepository.AddQuestionForChannelAsync(question);

            return question;
        }

        public async Task<Question> PostQuestionForVideoAsync(Question question)
        {
            question.id = System.Guid.NewGuid().ToString();
            question.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            question.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _questionRepository.AddQuestionForVideoAsync(question);

            return question;
        }

        public async Task SaveUserQuestionAsync(string userId, string questionId)
        {
            bool savedAlready = await _questionRepository.CheckIfQuetionIsSavedAlready(userId, questionId);
            if (!savedAlready)
            {
                await _questionRepository.SaveUserQuestionAsync(userId, questionId);
            }
        }

        public async Task UnSaveUserQuestionAsync(string userId, string questionId)
        {
            await _questionRepository.UnSaveUserQuestionAsync(userId, questionId);
        }
    }
}
