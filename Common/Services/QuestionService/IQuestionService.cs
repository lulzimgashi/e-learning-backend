using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;

namespace Services.QuestionService
{
    public interface IQuestionService
    {
        Task<IList<Question>> GetQuestionByParentIdAsync(string parentId);
        Task<IList<Question>> GetAllQuestionsAsync();
        Task<Question> GetSingleQuestionAsync(string questionId);
        Task<ChannelQuestionPayload> PostQuestionForChannelAsync(ChannelQuestionPayload question);
        Task<Question> PostQuestionForVideoAsync(Question question);
        Task SaveUserQuestionAsync(string userId, string questionId);
        Task UnSaveUserQuestionAsync(string userId, string questionId);
    }

}
