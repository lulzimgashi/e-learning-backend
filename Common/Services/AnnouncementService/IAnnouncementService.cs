

using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;

namespace Services.AnnouncementService
{
    public interface IAnnouncementService
    {
        Task<IList<Announcement>> GetAllAnnouncementsAsync();
        Task<Announcement> GetSingleAnnouncementAsync(string announcementId);
        Task<Announcement> AddNewAnnouncementAsync(Announcement announcement);
        Task SaveUserAnnouncementAsync(string userId, string announcementId);
        Task UnSaveUserAnnouncementAsync(string userId, string announcementId);
    }

}
