

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.Exceptions;
using Repositories.AnnouncementRepository;

namespace Services.AnnouncementService
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly IAnnouncementRepository _announcementRepository;

        public AnnouncementService(IAnnouncementRepository announcementRepository)
        {
            _announcementRepository = announcementRepository;
        }

        public async Task<Announcement> AddNewAnnouncementAsync(Announcement announcement)
        {
            announcement.id = System.Guid.NewGuid().ToString();
            announcement.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            announcement.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _announcementRepository.AddNewAnnouncementAsync(announcement);

            return announcement;
        }

        public async Task<IList<Announcement>> GetAllAnnouncementsAsync()
        {
            IList<Announcement> announcements = await _announcementRepository.FindAllAnnouncementsAsync();

            return announcements;
        }

        public async Task<Announcement> GetSingleAnnouncementAsync(string announcementId)
        {
            Announcement announcement = await _announcementRepository.FindSingleAnnouncementAsync(announcementId);
            if (announcement == null)
            {
                throw new EntityNotFoundException();
            }


            return announcement;
        }

        public async Task SaveUserAnnouncementAsync(string userId, string announcementId)
        {
            bool savedAlready = await _announcementRepository.CheckIfAnnouncmentIsSavedAlready(userId, announcementId);
            if (!savedAlready)
            {
                await _announcementRepository.SaveUserAnnouncementAsync(userId, announcementId);
            }
        }

        public async Task UnSaveUserAnnouncementAsync(string userId, string announcementId)
        {
            await _announcementRepository.UnSaveUserAnnouncementAsync(userId, announcementId);
        }
    }
}
