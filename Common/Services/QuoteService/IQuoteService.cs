using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;

namespace Services.QuoteService
{
    public interface IQuoteService
    {
        Task<IList<Quote>> GetAllQuotesAsync();
        Task SaveUserQuoteAsync(string userId, string quoteId);
        Task UnSaveUserQuoteAsync(string userId, string quoteId);
    }

}
