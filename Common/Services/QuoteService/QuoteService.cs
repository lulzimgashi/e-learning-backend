using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Models.PublicModels.Exceptions;
using Repositories.QuoteRepository;

namespace Services.QuoteService
{
    public class QuoteService : IQuoteService
    {
        private readonly IQuoteRepository _quoteRepository;

        public QuoteService(IQuoteRepository quoteRepository)
        {
            _quoteRepository = quoteRepository;
        }


        public async Task<IList<Quote>> GetAllQuotesAsync()
        {
            IList<Quote> quotes = await _quoteRepository.FindAllQuotesAsync();

            return quotes;
        }

        public async Task SaveUserQuoteAsync(string userId, string quoteId)
        {
            bool savedAlready = await _quoteRepository.CheckIfQuoteIsSavedAlready(userId, quoteId);
            if (!savedAlready)
            {
                await _quoteRepository.SaveUserQuoteAsync(userId, quoteId);
            }
        }

        public async Task UnSaveUserQuoteAsync(string userId, string quoteId)
        {
            await _quoteRepository.UnSaveUserQuoteAsync(userId, quoteId);
        }
    }
}
