
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Models.PublicModels;
using Models.PublicModels.DTO;
using Models.PublicModels.Exceptions;
using Repositories.CourseRepository;
using Repositories.VideoRepository;

namespace Services.CourseService
{

    public class CourseService : ICourseService
    {
        private readonly ICourseRepository _courseRepository;
        private readonly IVideoRepository _videoRepository;

        public CourseService(ICourseRepository courseRepository, IVideoRepository videoRepository)
        {
            _courseRepository = courseRepository;
            _videoRepository = videoRepository;
        }


        public async Task<IList<Course>> GetAllCoursesAsync()
        {
            IList<Course> courses = await _courseRepository.FindAllCoursesAsync();

            return courses.AsList();
        }

        public async Task<IList<Course>> GetAllCoursesAdminAsync(string channelId)
        {
            IList<Course> courses = await _courseRepository.FindAllCoursesForAdminAsync(channelId);

            return courses.AsList();
        }


        public async Task<IList<Course>> GetAllKidsCourseAsync()
        {
            IList<Course> courses = await _courseRepository.FindAllKidsCoursesAsync();

            return courses.AsList();
        }


        public async Task<Course> GetSingleCourseAsync(string courseId, string loggedUser)
        {
            Course course = await _courseRepository.FindSingleCourseAsync(courseId);
            if (course == null)
            {
                throw new EntityNotFoundException();
            }


            //get videos of course
            IList<RelatedVideo> videos = await _videoRepository.FindAllVideosByCourseId(courseId);
            course.videosList = videos;

            //check if user saved course already
            if (!string.IsNullOrWhiteSpace(loggedUser))
            {
                course.saved = await _courseRepository.CheckIfUserSavedCourse(course.id, loggedUser);
            }

            return course;
        }

        public async Task SaveUserCourseAsync(string userId, string courseId)
        {
            bool savedAlready = await _courseRepository.CheckIfUserSavedCourse(courseId, userId);
            if (!savedAlready)
            {
                await _courseRepository.SaveUserCourseAsync(userId, courseId);
            }
        }

        public async Task UnSaveUserCourseAsync(string userId, string courseId)
        {
            await _courseRepository.UnSaveUserCourseAsync(userId, courseId);
        }

        public async Task<Course> CreateCourseAsync(Course course)
        {
            course.id = System.Guid.NewGuid().ToString();
            course.createdAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            course.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _courseRepository.CreateCourseAsync(course);

            return course;
        }

        public async Task<Course> UpdateCourseAsync(Course course)
        {
            course.updatedAt = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            await _courseRepository.UpdateCourseAsync(course);

            return course;
        }

        public async Task DeleteCourseAsync(string courseId)
        {
            await _courseRepository.DeleteCourseAsync(courseId);
        }

        public async Task<Course> GetSingleCourseAdminAsync(string courseId)
        {
            Course course = await _courseRepository.FindSingleCourseAdminAsync(courseId);
            if (course == null)
            {
                throw new EntityNotFoundException();
            }

            return course;
        }
    }
}
