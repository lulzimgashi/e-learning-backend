using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.PublicModels;
using Repositories;
using Services.UserService;

namespace Services.CourseService
{
    public interface ICourseService
    {

        Task<IList<Course>> GetAllCoursesAsync();

        Task<Course> GetSingleCourseAsync(string courseId,string loggedUser);

        Task<IList<Course>> GetAllKidsCourseAsync();
        Task SaveUserCourseAsync(string userId, string courseId);
        Task UnSaveUserCourseAsync(string userId, string courseId);
        Task<IList<Course>> GetAllCoursesAdminAsync(string channelId);
        Task<Course> CreateCourseAsync(Course course);
        Task<Course> UpdateCourseAsync(Course course);
        Task DeleteCourseAsync(string courseId);
        Task<Course> GetSingleCourseAdminAsync(string courseId);
    }
}
